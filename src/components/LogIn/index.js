import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { ToastContainer, toast } from "react-toastify";

import "./custom.scss";
import "react-awesome-slider/dist/styles.css";
import "react-toastify/dist/ReactToastify.css";

import AwesomeSlider from "react-awesome-slider";
import LogInContainer from "./LoginContainer";
import LogInImage1 from "./images/1.png";
import LogInImage2 from "./images/2.png";
import LogInImage3 from "./images/3.png";
import withAutoplay from "react-awesome-slider/dist/autoplay";

export default function LogIn({
  logInSubmit,
  setPasswordValue,
  setuserNameValue,
}) {
  const logInSliceData = useSelector((state) => state.logInSlice);
  const { isErrorStatus, errorResponse } = logInSliceData;

  useEffect(() => {
    if (isErrorStatus === true) {
      let info = errorResponse.ModelState.info;
      info.forEach((inf) => {
        WarningNotify(inf);
      });
    }
  }, [isErrorStatus]);

  const AutoplaySlider = withAutoplay(AwesomeSlider);
  const slider = (
    <AutoplaySlider
      className="img-slider"
      play={true}
      cancelOnInteraction={false} // should stop playing on user interaction
      interval={3000}
      bullets={true}
      buttons={false}
      organicArrows={false}
    >
      <div data-src={LogInImage1} />
      <div data-src={LogInImage2} />
      <div data-src={LogInImage3} />
    </AutoplaySlider>
  );

  const WarningNotify = (msg) =>
    toast.error(msg, {
      position: "top-right",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

  <ToastContainer
    position="top-right"
    autoClose={3000}
    hideProgressBar={false}
    newestOnTop={false}
    closeOnClick
    rtl={false}
    pauseOnFocusLoss
    draggable
    pauseOnHover
  />;

  return (
    <div className="main-wrapper">
      <div className="left-wrapper">
        {slider}
        <div className="log-in-text">
          <span className="text-other">
            The most trusted{" "}
            <span className="red-text">Trade & construction products</span>{" "}
            supplier in Australia
          </span>
        </div>
      </div>
      <div className="right-wrapper">
        <div className="dev1"></div>
        <LogInContainer
          logInSubmit={logInSubmit}
          setPasswordValue={setPasswordValue}
          setuserNameValue={setuserNameValue}
        />

        <div className="footer">
          <div>
            <span>© 2021 RLA Polymers</span>
          </div>
          <div className="footer-lable">
            <span>Terms & Conditions</span>
            <span>Cookie Policy</span>
            <span>Support</span>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
}
