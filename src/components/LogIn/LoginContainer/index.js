import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import "./custom.scss";
import ShowEye from "./showEye";
import Logo from "../images/logo-img.png";

export default function LogInContainer({
  logInSubmit,
  setPasswordValue,
  setuserNameValue,
}) {
  const [showEyeVale, setshowEyeVale] = useState(false);
  const [iconTypeValues, seticonTypeValues] = useState(false);
  const [pssType, setpssType] = useState(true);
  const [pssTypeText, setpssTypeText] = useState("password");
  const [buttonStatus, setbuttonStatus] = useState(true);

  const logInSliceData = useSelector((state) => state.logInSlice);
  const { isLoadingData } = logInSliceData;

  const [uLenth, setuLenth] = useState(0);
  const [pLenth, setpLenth] = useState(0);

  const handlePassword = (pass) => {
    setPasswordValue(pass);
    setpLenth(pass.length);
    let n = pass.length;
    buttonHandler();
    if (n > 1) {
      setshowEyeVale(true);
    } else {
      setshowEyeVale(false);
    }
  };

  const handleUserName = (uName) => {
    setuLenth(uName.length);
    setuserNameValue(uName);
    buttonHandler();
  };

  const buttonHandler = (e) => {
    if (uLenth > 0 && pLenth > 0) {
      setbuttonStatus(false);
    } else {
      setbuttonStatus(true);
    }
  };

  useEffect(() => {
    buttonHandler();
  }, [uLenth, pLenth]);

  useEffect(() => {
    if (pssType === true) {
      setpssTypeText("password");
      seticonTypeValues(!iconTypeValues);
    } else {
      setpssTypeText("text");
      seticonTypeValues(!iconTypeValues);
    }
  }, [pssType]);

  return (
    <div className="login-wrapper">
      <div className="logo-mini">
        <img src={Logo} alt="logo" className="logo-img" />
      </div>
      <div className="hedder">
        <span className="hedder-text">Welcome Back</span>
        <span className="hedder-tagline">
          Please log in to start purchasing
        </span>
      </div>

      <div className="input-wrapper">
        <div className="form-input">
          <input
            onChange={(e) => {
              handleUserName(e.target.value);
            }}
            className="input-username"
            required=" "
            autoComplete="off"
          />
          <span className="user-lable">Email Address</span>
        </div>

        <div className="form-input">
          <input
            onChange={(e) => {
              handlePassword(e.target.value);
            }}
            className="input-password"
            required=" "
            type={pssTypeText}
            autoComplete="new-password"
          />
          <span className="pass-lable">Password</span>
          {showEyeVale ? (
            <ShowEye
              iconTypeValue={iconTypeValues}
              setpssType={setpssType}
              pssType={pssType}
            />
          ) : (
            <div></div>
          )}
        </div>
      </div>

      <div className="remember-wrapper">
        <div className="check-box-wrapper">
          <lable className="container">
            <input type="checkbox" />
            <span class="checkmark"></span>
          </lable>
          <span className="remember-me-text"> Remember Me</span>
        </div>
        <div>
          <span className="forgot-password">Forgot Password?</span>
        </div>
      </div>
      <div className="button-wrapper">
        <button
          disabled={buttonStatus}
          className="button"
          onClick={() => {
            logInSubmit();
          }}
        >
          <span
            className={
              isLoadingData ? "button__text  button--loading" : "button__text"
            }
          >
            {isLoadingData ? "" : "LogIn"}
          </span>
        </button>
      </div>
      <div className="sign-up-text">
        Don’t have a RLA Polymer account yet?{" "}
        <span className="sign-up-text-red">SIGN UP </span> Now
      </div>
    </div>
  );
}
