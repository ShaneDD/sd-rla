import React from "react";
import "./custom.scss";
import { RiEyeFill, RiEyeOffFill } from "react-icons/ri";

export default function ShowEye({ iconTypeValue, setpssType, pssType }) {
  return (
    <div className="home-check">
      {iconTypeValue ? (
        <RiEyeFill
          className="pass-eye"
          onClick={() => {
            setpssType(!pssType);
          }}
        />
      ) : (
        <RiEyeOffFill
          className="pass-eye"
          onClick={() => {
            setpssType(!pssType);
          }}
        />
      )}
    </div>
  );
}
