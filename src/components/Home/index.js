import React from "react";
import "./custom.scss";
import Footer from "./HomeSub/Footer";
import Header from "./HomeSub/Header";
import Main from "./HomeSub/Main";
import NavBar from "./HomeSub/NavBar";

function Home() {
  return (
    <div className="grid-contianer">
      <Header />
      <NavBar />
      <Main />
      <Footer />
    </div>
  );
}

export default Home;
