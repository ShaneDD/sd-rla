import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { exportExcel, filterKeyWord } from "../../../store/actions/flilterAction";
import { listProducts } from "../../../store/actions/homeActions";
import ProductToggleBtn from "./ProductToggleBtn";

function SearchBar() {

  const dispatch = useDispatch();

  const handleChange = (e) => {
    let keyword = e.target.value;
    let subdivisionCode = "";
    let divisionCode = "";
    dispatch(filterKeyWord(keyword));
    dispatch(listProducts(divisionCode, subdivisionCode, keyword));
    // do whatever you want with isChecked value
  };

  const exportExcelsheet = useSelector(state => state.exportExcelsheet);

  const { excel } = exportExcelsheet;

  const exoprtExcelSheet = () => {
    dispatch(exportExcel());
    var byteArray = excel.ByteArray;
    const binary = atob(byteArray.replace(/\s/g, ''));
    const len = binary.length;
    const buffer = new ArrayBuffer(len);
    const view = new Uint8Array(buffer);
    
    for(let i=0; i<len;i++){
      view[i] = binary.charCodeAt(i);
    }

    const blob = new Blob([view], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
    // console.log(blob);
    const url = URL.createObjectURL(blob);
    window.open(url, '_blank');
  }

  return (
    <div className="search-pro row">
      <div className="search-form2">
        <form action className="search-action">
          <input
            type="search"
            id="search"
            placeholder=" Search Products"
            className="search-input"
            style={{ backgroundImage: `url(` + `'./images/search.png'` + `)` }}
            onChange={(e) => handleChange(e)}
          />
        </form>
      </div>
      <div className="sort-per">
        <div className="sort-per-title">
          <p>Sort By</p>
          <select name id>
            <option value selected>
              Relevant
            </option>
          </select>
        </div>
        <div className="sort-per-title">
          <p>Per Page</p>
          <select name id>
            <option value selected>
              10
            </option>
          </select>
        </div>
      </div>
      <ProductToggleBtn />
      <div className="excelexport">
        <button type="submit" class="btn btn-success" onClick={()=>exoprtExcelSheet()}>
          &nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i>
          &nbsp; Export to Excel
        </button>
        {/* <input type="button" defaultValue="Export to Excel" /> */}
      </div>
    </div>
  );
}

export default SearchBar;
