import React from "react";
import MainImage from "./MainImage";
import MainNavBar from "./MainNavBar";

function NavBar() {
  return (
    <nav className="main-main-nav-bar">
      <MainNavBar />
      <div className="nav-bar">
        <ul className="menu">
          <li>
            <a href>Trade &amp; Construction Products</a>
          </li>
          <li>
            <a href>Industrial Solutions</a>
          </li>
          <li>
            <a href>Latest News</a>
          </li>
          <li>
            <a href>Resources</a>
          </li>
        </ul>
      </div>
      <MainImage />
    </nav>
  );
}

export default NavBar;
