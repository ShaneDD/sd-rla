import React from "react";
import { useDispatch } from "react-redux";
import { clickSubDivitionFilter } from "../../../store/actions/flilterAction";
import { listProducts } from "../../../store/actions/homeActions";

function Subsubcategories(props) {
  const { subdivisions, divisionName, divisionCode } = props;

  const dispatch = useDispatch();

  const handleChange = (e) => {
    let isChecked = e.target.checked;
    if (isChecked) {
      const filterData = {
        division: {
          name: divisionName,
          code: divisionCode,
        },
        subdivision: {
          name: e.target.name,
          code: e.target.value,
        },
      };
      var keyword = "";
      dispatch(clickSubDivitionFilter(filterData));
      dispatch(listProducts(divisionCode, e.target.value, keyword));
    }
    // do whatever you want with isChecked value
  };

  return (
    <div className="subchecklist subsubchecklist">
      {subdivisions.map((subdivision, index) => (
        <div key={index}>
          <input
            type="checkbox"
            id="vehicle1"
            name={subdivision.description}
            value={subdivision.Code}
            onChange={(e) => handleChange(e)}
          />
          <label for="vehicle1">{subdivision.description}</label>
        </div>
      ))}
    </div>
  );
}

export default Subsubcategories;
