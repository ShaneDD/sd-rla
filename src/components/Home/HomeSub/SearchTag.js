import React, { useState } from "react";
import { useSelector } from "react-redux";

function SearchTag(props) {
  var [isClose, setIsClose] = useState(true);

  const handleChange = () => {
    setIsClose(false);
  };

  return (
    <>
      {!isClose ? (
        <></>
      ) : (
        <div className="card">
          {props.name}&nbsp;
          <svg
            width={10}
            height={10}
            viewBox="0 0 10 10"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            style={{ cursor: "pointer" }}
            onClick={() => handleChange()}
          >
            <path
              d="M5.00005 3.93951L8.71255 0.227005L9.77305 1.28751L6.06055 5.00001L9.77305 8.71251L8.71255 9.77301L5.00005 6.0605L1.28755 9.77301L0.227051 8.71251L3.93955 5.00001L0.227051 1.28751L1.28755 0.227005L5.00005 3.93951Z"
              fill="#959595"
            />
          </svg>
        </div>
      )}
    </>
  );
}

export default SearchTag;
