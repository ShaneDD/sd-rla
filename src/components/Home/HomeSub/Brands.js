import React, { useState } from "react";
import SubCategories from "./SubCategories";

function Brands(props) {
  const [categorilist, setCategorilist] = useState(false);
  const { filters } = props;

  return (
    <>
      <div className="Categories">
        <div className="categoryTitle">{props.filterTitle}</div>
        <div className="downarrow">
          {categorilist === false ? (
            <i
              class="fa fa-chevron-down"
              aria-hidden="false"
              onClick={() => setCategorilist(true)}
            ></i>
          ) : (
            <i
              class="fa fa-times-circle"
              aria-hidden="true"
              onClick={() => setCategorilist(false)}
            ></i>
          )}
        </div>
      </div>
      {categorilist === false ? (
        <div></div>
      ) : (
        <div className="subcatergories">
          <>
            <div className="search-form2 search-form2-brand">
              <form action className="search-action">
                <input
                  type="search"
                  id="search"
                  placeholder=" Search Brands"
                  className="search-input"
                  style={{
                    backgroundImage: `url(` + `'./images/search.png'` + `)`,
                    backgroundColor: '#f2f2f2'
                  }}
                //   onChange={(e) => handleChange(e)}
                />
              </form>
            </div>
          </>
          <>
            <div className="subchecklist">
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> HoldFast</label>
              </div>
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> Komponent</label>
              </div>
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> Cementitious</label>
              </div>
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> Anchorbolt</label>
              </div>
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> Beckothane</label>
              </div>
            </div>
          </>
        </div>
      )}
      <hr />
    </>
  );
}

export default Brands;
