import React, { useState } from "react";
import Subsubcategories from "./Subsubcategories";

function SubCategories(props) {
  const [indexState, setIndexState] = useState('');
  const { filters } = props;

  return (
    <div>
      {filters.divisions.map((division, index) =>
        indexState !== division.description ? (
          <div key={index} className="subsubcatergories">
            <div className="subcategoryTitle">{division.description}</div>
            <div className="downarrow">
              <i
                class="fa fa-chevron-down"
                aria-hidden="false"
                onClick={(e) => {
                  setIndexState(division.description);
                  console.log(indexState);
                }}
              ></i>
            </div>
          </div>
        ) : (
          <>
            <div key={index} className="subsubcatergories sub-subDetails">
              <div className="subcategoryTitle">{division.description}</div>
              <div className="downarrow">
                <i
                  class="fa fa-times-circle"
                  aria-hidden="false"
                  onClick={(e) => {
                    setIndexState('')
                    console.log(indexState);
                  }}
                ></i>
              </div>
            </div>
            <Subsubcategories
              divisionName={division.description}
              divisionCode={division.Code}
              subdivisions={division.subdivisions}
            />
          </>
        )
      )}
    </div>
  );
}

export default SubCategories;
