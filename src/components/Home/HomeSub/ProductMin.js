import React from "react";

function ProductMin(props) {
  const { product } = props;

  return (
    <div className="products row mini-product">
      <div className="product-img">
        <a href={product.Detail} target="_blank">
          <i
            className="fa fa-heart-o"
            style={{ color: "#d42e12" }}
            aria-hidden="true"
          />
        </a>
        <a href={product.Detail} target="_blank">
          <img src={product.Image} alt="" />
        </a>
      </div>
      <div className="offer">
        <div className="precentage">
          {" "}
          {Math.round(product.DiscountPercentage)}%
        </div>
        <div>OFF</div>
      </div>
      <div className="product-details">
        <a href={product.Detail} target="_blank">
          <div className="title-products">
            <p className="main-title">{product.description}</p>
            <p>
              Item Code : <strong>{product.ItemNumber}</strong>
            </p>
          </div>
        </a>
      </div>
      <div className="product-cost-details min-detials-cost">
        <div>
          <div className="product-cost">
            <p className="dis-cost">
              ${Number(product.NationalPrice).toFixed(2)}&nbsp;
              <i class="fa fa-info-circle" aria-hidden="true"></i>
            </p>
            <p className="cost-pro">
              ${Number(product.SpecialPrice).toFixed(2)}
            </p>
          </div>
          <p className="cost2">$ 101 incl. GST (AUD)</p>
        </div>
        <div className="qty qty-min">
          <input type="number" name="qty" value="1" id="qty" />
        </div>
        <button type="submit" class="btn btn-success">
          <i class="fa fa-shopping-cart fa-lg" aria-hidden="true"></i>
        </button>
      </div>
    </div>
  );
}

export default ProductMin;
