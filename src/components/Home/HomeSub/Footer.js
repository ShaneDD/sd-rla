import React from "react";

function Footer() {
  return (
    <footer className="row">
      <div className="footer-details">
        <div className="logo">
          <img src="./images/logo-black-text.png" alt="" />
        </div>
        <div className="paragraph">
          RLA Polymers provide Australian manufactured products, for the
          Australian construction, building and trade industries. We provide
          tailor made solutions to get your project complete efficiently and on
          time.
        </div>
      </div>
      <div className="footer-links">
        <div className="foot-link-title">Useful Links</div>
        <hr />
        <div className="footer-link-list">
          <div>
            <ul>
              <li>Home</li>
              <li>Tile & Construction Products</li>
              <li>Industrial Solutions</li>
              <li>Resources</li>
            </ul>
          </div>
          <div>
            <ul>
              <li>Latest News</li>
              <li>About RLA</li>
              <li>Contact Us</li>
              <li>Privacy Policy</li>
            </ul>
          </div>
        </div>
      </div>
      <div className="footer-contacts">
        <div className="foot-link-title">Useful Links</div>
        <hr />
        <div className="cantactNmber">
          <i class="fa fa-phone" aria-hidden="true"></i> 1800 242 931
        </div>
        <div className="address">
          <i class="fa fa-map-marker" aria-hidden="true"></i>
          215 Colchester Road, Kilsyth, Victoria, 3137
        </div>
        <div className="emailaddress">
          <i class="fa fa-envelope-o emailicon" aria-hidden="true"></i>
          sales@rlapolymers.com.au
        </div>
      </div>
    </footer>
  );
}

export default Footer;
