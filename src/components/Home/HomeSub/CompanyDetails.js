import React, { useEffect } from "react";
import CompanyDetailsSub from "./CompanyDetailsSub";
import ReactWeather, { useOpenWeather } from "react-open-weather";

function CompanyDetails(props) {
  const { data, isLoading, errorMessage } = useOpenWeather({
    key: "95b5cffd3d2f820ecfd42263db170da4",
    lat: "48.137154",
    lon: "11.576124",
    lang: "en",
    unit: "metric", // values are (metric, standard, imperial)
  });  

  const {products} = props;
  
  return (
    <div className="company-details row">
      <div className="company-logo">
        <img src='./images/company-logo.png' alt="company-logo" />
      </div>
      <div className="company-details-colum">
        <div className="user_details">
          <div className="account-setting">
            <div>
              <svg
                width={40}
                height={34}
                viewBox="0 0 40 34"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M20 0C8.97186 0 0 8.97186 0 20C0 25.1025 1.92169 29.7638 5.07782 33.3011C5.75378 30.3329 7.30499 27.6248 9.57001 25.5151C11.1597 24.0344 13.0197 22.9263 15.0302 22.2372C12.4179 20.5841 10.6796 17.6694 10.6796 14.3561C10.6796 9.21661 14.8608 5.03571 20 5.03571C25.1392 5.03571 29.3201 9.21661 29.3201 14.3561C29.3201 17.6678 27.5833 20.5817 24.9728 22.2351C27.1506 22.981 29.1467 24.2194 30.8224 25.8951C32.8699 27.9425 34.2834 30.5161 34.9207 33.3023C38.078 29.765 40 25.1031 40 20C40 8.97186 31.0281 0 20 0Z"
                  fill="#DBDBDB"
                />
              </svg>
            </div>
            <div className="def-val">
              <p>Welcome {products.Customer}</p>
              <a href className="company-email">
                John.doe@allyatiles.com
              </a>
            </div>
          </div>
          <div className="order-details row">
            <CompanyDetailsSub count={46} name="Total Orders" />
            <CompanyDetailsSub count={37} name="Awaiting Delivery" />
            <CompanyDetailsSub count={0} name="Pending Payment" border="none" />
          </div>
        </div>
      </div>
      {isLoading ? (
        <div className="wheather row">Loading...</div>
      ) : (
        <div className="wheather row">
          <div className="wheather-details">
            <p>
              Queensland <i class="fa fa-location-arrow" aria-hidden="true"></i>
            </p>
            <p className="temprature">20&#176;</p>
            <p>Most Cloudy</p>
          </div>
          <div className="wheather-img">
            <svg
              width="23"
              height="24"
              viewBox="0 0 23 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M11.3929 0.821419C11.3929 7.30542 6.59135 12.1071 0.107178 12.1071C6.59135 12.1071 11.3929 16.9081 11.3929 23.3929C11.3929 16.9081 16.1944 12.1071 22.6786 12.1071C16.1944 12.1071 11.3929 7.30524 11.3929 0.821419Z"
                fill="#FCD95F"
              />
            </svg>

            <svg
              width={79}
              height={46}
              viewBox="0 0 79 46"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M62.0714 11.3215C61.086 11.3215 60.1262 11.4234 59.1874 11.5849C55.3224 4.69727 47.9602 0.0357437 39.5 0.0357437C31.0398 0.0357437 23.6778 4.6971 19.8126 11.5849C18.8738 11.4234 17.914 11.3215 16.9286 11.3215C7.57924 11.3215 0 18.9007 0 28.25C0 37.5994 7.57924 45.1786 16.9286 45.1786H62.0714C71.4208 45.1786 79 37.5994 79 28.25C79 18.9007 71.4208 11.3215 62.0714 11.3215Z"
                fill="#F3F3F3"
              />
            </svg>
          </div>
        </div>
      )}
    </div>
  );
}

export default CompanyDetails;
