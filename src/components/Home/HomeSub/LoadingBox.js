import React from "react";

function LoadingBox() {
  return (
    <div class="loader"></div>
  );
}

export default LoadingBox;
