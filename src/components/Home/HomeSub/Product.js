import React from "react";
import data from "../../../data";

function Product(props) {
  const { product } = props;

  return (
    <div className="products row">
      <div className="product-img">
        <a href={product.Detail} target="_blank">
          <i
            className="fa fa-heart-o"
            style={{ color: "red" }}
            aria-hidden="true"
          />
        </a>
        <a href={product.Detail} target="_blank">
          <img src={product.Image} alt="" />
        </a>
      </div>
      {Math.round(product.DiscountedPrice) === 0 ? (
        <></>
      ) : (
        <div className="offer">
          <div className="precentage">
            {Math.round(product.DiscountPercentage)}%
          </div>
          <div>OFF</div>
        </div>
      )}
      <div className="product-details">
        <a href={product.Detail} target="_blank">
          <div className="title-products">
            <p className="main-title">{product.description}</p>
            <p>
              Item Code : <strong>{product.ItemNumber}</strong>
            </p>
          </div>
        </a>
        <a href={product.Detail} target="_blank">
          <div className="title-details row">
            <div>
              <p>
                Item Type: <strong>{product.ItemType}</strong>
              </p>
              <p>
                Item Sub-Division: <strong>{product.ItemSubDivision}</strong>
              </p>
              <p>
                Item Major-Group: <strong>{product.ItemMinorGroup}</strong>
              </p>
            </div>
            <div>
              <p>
                Effective Date: <strong>{product.effectiveDate}</strong>
              </p>
              <p>
                Item Minor Group: <strong>{product.ItemMajorGroup}</strong>
              </p>
              <p>
                Sales Persons: <strong>{product.SalesPerson}</strong>
              </p>
            </div>
          </div>
        </a>
      </div>
      <div className="product-cost-details">
        <p className="dis-cost">
          ${Number(product.NationalPrice).toFixed(2)}&nbsp;
          <i class="fa fa-info-circle" aria-hidden="true"></i>
        </p>
        <p className="cost-pro">${Number(product.SpecialPrice).toFixed(2)}</p>
        <p className="cost2">$ 101 incl. GST (AUD)</p>
        <div className="qty">
          Qty:&nbsp;&nbsp;
          <input type="number" name="qty" value="1" id="qty" />
        </div>
        <input
          type="button"
          defaultValue="ADD TO CART"
          className="addtoccart"
        />
      </div>
    </div>
  );
}

export default Product;
