import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { clickToggleMainBtn } from "../../../store/actions/homeActions";

function ProductToggleBtn() {
  const [className, setClassName] = useState("shortView");

  const dispatch = useDispatch();
  const clickToggleMain = (longView) =>{
    dispatch(clickToggleMainBtn(longView));
    setClassName(longView);
  }

  return (
    <div className="toggle-view">
      <button
        className={
          "view-btn list-view" + (className === "longView" ? " is-active" : "")
        }
        onClick={() => clickToggleMain('longView')}
      >
        <span></span>
        <span></span>
        <span></span>
      </button>

      <button
        className={
          "view-btn list-view" + (className === "shortView" ? " is-active" : "")
        }
        onClick={() => clickToggleMain("shortView")}
      >
        <span></span>
        <span></span>
      </button>
    </div>
  );
}

export default ProductToggleBtn;
