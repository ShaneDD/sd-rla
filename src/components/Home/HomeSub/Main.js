import React, { useEffect, useState } from "react";
import CompanyDetails from "./CompanyDetails";
import SearchTag from "./SearchTag";
import SearchBar from "./SearchBar";
import Product from "./Product";
// import data from "../../../data";
import Filter from "./Filter";
import ProductMin from "./ProductMin";
import axios from "axios";
import LoadingBox from "./LoadingBox";
import { useDispatch, useSelector } from "react-redux";
import { listProducts } from "../../../store/actions/homeActions";

import ContentLoader, { Facebook } from "react-content-loader";

const MyFacebookLoader = () => <Facebook />;

const MyLoader = () => (
  <ContentLoader viewBox="10 4 380 50">
    {/* Only SVG shapes */}
    <rect x="10" y="20" rx="5" ry="5" width="30" height="30" />
    <rect x="80" y="17" rx="4" ry="4" width="300" height="8" />
    <rect x="80" y="40" rx="3" ry="3" width="250" height="8" />
  </ContentLoader>
);

function Main() {
  const isClickToggle = useSelector((state) => state.toggleButton);

  const dispatch = useDispatch();
  const productList = useSelector((state) => state.productList);
  const { error, products } = productList;

  const loading = true;

  const toggleClick = useSelector((state) => state.toggleButton);

  const flits = useSelector((state) => state.clickFilter);

  var subdivision = "";
  var subdivisionCode = "";
  var devisionCode = "";
  if (flits.isClick !== undefined) {
    subdivision = flits.isClick.subdivision;
    devisionCode = flits.isClick.division.code;
    subdivisionCode = flits.isClick.subdivision.code;
  }

  const keyWord = useSelector((state) => state.keyWord);
  const { keyword } = keyWord;

  const {
    isClick: { className },
  } = toggleClick;

  useEffect(() => {
    dispatch(listProducts(devisionCode, subdivisionCode, keyword));
  }, [dispatch]);

  return (
    <main>
      {loading ? <LoadingBox /> : <CompanyDetails products={products} />}
      <div className="main-body">
        <Filter isClose />
        <div className="main-body-body">
          <div
            className="ads"
            style={{ backgroundImage: `url(` + `'./images/ads.png'` + `)` }}
          >
            {/* <img src={ads} alt="" /> */}
          </div>
          <div className="search-cards">
            {subdivision !== "" ? <SearchTag name={subdivision.name} /> : <></>}
          </div>
          <SearchBar />
          {loading || products.length === 0 ? (
            <div>
              <MyLoader />
              <MyLoader />
              <MyLoader />
            </div>
          ) : error ? (
            <div>
              <MyLoader />
              <MyLoader />
              <MyLoader />
            </div>
          ) : (
            <>
              {products.Items.map((product) =>
                className === "longView" ? (
                  <Product key={product._id} product={product} />
                ) : (
                  <ProductMin key={product._id} product={product} />
                )
              )}
            </>
          )}

          <div>
            <div className="page-box-wrapper">
              <div className="page-box">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
              </div>
              <div className="page-box-selected">1</div>
              <div className="page-box">2</div>
              <div className="page-box">3</div>
              <div className="page-box">4</div>
              <div className="page-box">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default Main;
