import React from "react";

function MainImage() {
  return (
    <div className="main-image" style={{backgroundImage: `url(`+`'./images/bg.png'`+`)`}}>
      <div className="title-main">
        <p>Trade &amp; Contruction Products</p>
      </div>
      <div className="description">
        <p>
          <a href="#">Home</a> / Trade &amp; Construction Products
        </p>
      </div>
    </div>
  );
}

export default MainImage;
