import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import FilterSub from "./FilterSub";
import { filterList } from "../../../store/actions/flilterAction";
import Brands from "./Brands";

function Filter() {
  const dispatch = useDispatch();
  const filterDropDownList = useSelector((state) => state.filterDropDownList);
  const { filters, loading, error } = filterDropDownList;
  useEffect(() => {
    dispatch(filterList());
  }, [dispatch]);

  return (
    <div className="filter">
      <div className="title">
        <p className="t">Filters</p>
        <p className="r">Reset</p>
      </div>

      <FilterSub filters={filters} filterTitle="CATEGORIES" />
      <Brands filters={filters} filterTitle="BRANDS" />
      {/* <FilterSub filters={filters} filterTitle="PRICE" /> */}
      <div className="Categories">
        <div className="categoryTitle">PRICES</div>
        <div className="downarrow">
            <i
              class="fa fa-chevron-down"
              aria-hidden="false"
              // onClick={() => setCategorilist(true)}
            ></i>
        </div>
      </div>
    </div>
  );
}

export default Filter;
