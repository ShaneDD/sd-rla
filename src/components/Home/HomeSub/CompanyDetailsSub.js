import React from "react";

function CompanyDetailsSub(props) {
  return (
    <div className="order-count" style={{border: props.border}}>
      <p className="count">{props.count}</p>
      <p className="order-title">{props.name}</p>
    </div>
  );
}

export default CompanyDetailsSub;
