import React, { useState } from "react";
import SubCategories from "./SubCategories";

function FilterSub(props) {
  const [categorilist, setCategorilist] = useState(false);
  const { filters } = props;

  return (
    <>
      <div className="Categories">
        <div className="categoryTitle">{props.filterTitle}</div>
        <div className="downarrow">
          {categorilist === false ? (
            <i
              class="fa fa-chevron-down"
              aria-hidden="false"
              onClick={() => setCategorilist(true)}
            ></i>
          ) : (
            <i
              class="fa fa-times-circle"
              aria-hidden="true"
              onClick={() => setCategorilist(false)}
            ></i>
          )}
        </div>
      </div>
      {categorilist === false ? (
        <div></div>
      ) : (
        <div className="subcatergories">
          <>
            <SubCategories filters = {filters} />
          </>
          <>
            <div className="subchecklist">
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> Construction Adhesives 21</label>
              </div>
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> Repair Mortar 14</label>
              </div>
              <div>
                <input
                  type="checkbox"
                  id="vehicle1"
                  name="vehicle1"
                  value="Bike"
                />
                <label for="vehicle1"> Coatings & Epoxy 11</label>
              </div>
            </div>
          </>
        </div>
      )}
      <hr />
    </>
  );
}

export default FilterSub;
