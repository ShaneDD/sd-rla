import { combineReducers } from "redux";
import logInSlice from "./logInSlice";
import { productListReducer, toggelButtonReducer } from "./homeReducers";
import {
  exportExcelReducer,
  filterClickReducer,
  filterKeyWordReducer,
  filterListReducer,
} from "./filterReducers";

const appReducer = combineReducers({
  logInSlice: logInSlice,
  productList: productListReducer,
  toggleButton: toggelButtonReducer,
  filterDropDownList: filterListReducer,
  clickFilter: filterClickReducer,
  keyWord: filterKeyWordReducer,
  exportExcelsheet:exportExcelReducer,
});

export const rootReducer = (state, action) => {
  if (action.type === "USER_LOGOUT") {
    state = undefined;
  }

  return appReducer(state, action);
};
