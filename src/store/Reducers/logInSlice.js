import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const networkError = {
  data: {
    Message: "Network Error.",
    ModelState: {
      info: ["Unable to Establish the Connection"],
    },
  },
};

export const logInSlice = createSlice({
  name: "logInSlice",
  initialState: {
    logInSlice: [],
    isLoadingData: false,
    isLoggedIn: false,
    userData: [],
    isErrorStatus: false,
    errorResponse: [],
  },
  reducers: {
    dataSubmiting(state, action) {
      // Use a "state machine" approach for loading state instead of booleans
      if (state.isLoadingData === false) {
        state.isLoadingData = true;
        state.isErrorStatus = false;
      }
    },
    userDataReceived(state, action) {
      if (state.isLoadingData === true) {
        state.isLoadingData = false;
        state.userData = action.payload.data;
        state.isLoggedIn = true;
        state.isErrorStatus = false;
      }
    },

    errorReceived(state, action) {
      if (state.isLoadingData === true) {
        state.isLoadingData = false;
        state.isErrorStatus = true;
        state.errorResponse = action.payload.data;
      }
    },
  },
});

export const { dataSubmiting, userDataReceived, errorReceived } =
  logInSlice.actions;

//Get
export const signInnn = () => async (dispatch) => {
  dispatch(dataSubmiting());
  const response = await axios.get(`https://api.github.com/users/mapbox`);
  dispatch(userDataReceived(response.data));
};

// Sample post Request
export const signIn = (uName, pWord) => async (dispatch) => {
  dispatch(dataSubmiting());
  axios
    .post(
      `https://posappdev-native.nzsafetyblackwoods.co.nz/api/v2.0/Customer/Login`,
      {
        Email: uName,
        Password: pWord,
      }
    )
    .then(function (response) {
      console.log(response);
      dispatch(userDataReceived(response));
    })
    .catch((err) => {
      if (err.response) {
        // client received an error response (5xx, 4xx)
        dispatch(errorReceived(err.response));
      } else if (err.request) {
        // client never received a response, or request never left
        dispatch(errorReceived(networkError));
      } else {
        // anything else
      }
    });
};

export default logInSlice.reducer;
