export const productListReducer = (state = {products:[]}, action) => {
  switch (action.type) {
    case "PRODUCT_LIST_REQUEST":
      return { loading: true };
    case "PRODUCT_LIST_SUCCESS":
      return { loading: false, products: action.payload };
    case "PRODUCT_LIST_FAIL":
        return{loading:false, error: action.payload}
    default:
      return state;
  }
};

export const toggelButtonReducer = (state = {isClick:[]}, action) => {
  switch (action.type) {
    case "CLICK_MAIN":
        return {isClick: action.payload};
    case "CLICK_SUMMARY":
        return {isClick: action.payload};
    default:
      return state;
  }
}
