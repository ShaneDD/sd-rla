export const filterListReducer = (state = {filters:[]}, action) => {
    switch (action.type) {
      case "FILTER_LIST_REQUEST":
        return { loading: true };
      case "FILTER_LIST_SUCCESS":
        return { loading: false, filters: action.payload };
      case "FILTER_LIST_FAIL":
          return{loading:false, error: action.payload}
      default:
        return state;
    }
  };

  export const filterClickReducer = (state = {filts:[]}, action) => {
    switch (action.type) {
      case "CLICK_FILTER":
          return {isClick: action.payload};
      default:
        return state;
    }
  }

  export const filterKeyWordReducer = (state = {keyword:[]}, action) => {
    switch (action.type) {
      case "TTPE_KEYWORD":
          return {keyword: action.payload};
      default:
        return state;
    }
  }

  export const exportExcelReducer = (state = {excel:[]}, action) => {
    switch (action.type) {
      case "EXCEL_EXPORT_REQUEST":
        return { loading: true };
      case "EXCEL_EXPORT_SUCCESS":
        return { loading: false, excel: action.payload };
      case "EXCEL_EXPORT_FAIL":
          return{loading:false, error: action.payload}
      default:
        return state;
    }
  };