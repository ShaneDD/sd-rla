import Axios from "axios";

export const filterList = () => async (dispatch) => {
  dispatch({
    type: "FILTER_LIST_REQUEST",
  });
  try {
    const { data } = await Axios.get(
      "https://posappdev-native.nzsafetyblackwoods.co.nz/api/v2.0/Customer/GetDivisions"
    );
    dispatch({
      type: "FILTER_LIST_SUCCESS",
      payload: data,
    });
  } catch (error) {
    dispatch({ type: "FILTER_LIST_FAIL", payload: error.message });
  }
};

export const clickSubDivitionFilter =(filt)=> async (dispatch) => {
  dispatch({
    type: "CLICK_FILTER",
    payload: filt,
  });
};

export const filterKeyWord =(keyword)=> async (dispatch) => {
  dispatch({
    type: "TTPE_KEYWORD",
    payload: keyword,
  });
};

export const exportExcel = () => async (dispatch) => {
  dispatch({
    type: "EXCEL_EXPORT_REQUEST",
  });
  try {
    const { data } = await Axios.post(
      " https://posappdev-native.nzsafetyblackwoods.co.nz/api/v2.0/Customer/DownloadProductExcel",
      {
        Email: "john.doe@allyatiles.com",
        DivisionCode: "",
        SubDivisionCode: "",
        ItemCode: "",
        SearchKeyword: '',
        Mode: "MAIN",
      }
    );
    dispatch({
      type: "EXCEL_EXPORT_SUCCESS",
      payload: data,
    });
  } catch (error) {
    dispatch({ type: "EXCEL_EXPORT_FAIL", payload: error.message });
  }
};

