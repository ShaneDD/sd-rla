import Axios from "axios";

export const listProducts =
  (divisionCode, subdivisionCode, keyword) => async (dispatch) => {
    dispatch({
      type: "PRODUCT_LIST_REQUEST",
    });
    try {
      const { data } = await Axios.post(
        "https://posappdev-native.nzsafetyblackwoods.co.nz/api/v2.0/Customer/GetProductsByCustomer",
        {
          Email: "john.doe@allyatiles.com",
          DivisionCode: divisionCode,
          SubDivisionCode: subdivisionCode,
          ItemCode: "",
          SearchKeyword: 'keyword',
          Mode: "MAIN",
        }
      );
      dispatch({
        type: "PRODUCT_LIST_SUCCESS",
        payload: data,
      });
    } catch (error) {
      dispatch({ type: "PRODUCT_LIST_FAIL", payload: error.message });
    }
  };

export const clickToggleMainBtn = (longView) => async (dispatch) => {
  dispatch({
    type: "CLICK_MAIN",
    payload: { className: longView },
  });
};
