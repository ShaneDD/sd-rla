import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";
import {
    exportExcelReducer,
  filterClickReducer,
  filterKeyWordReducer,
  filterListReducer,
} from "./store/Reducers/filterReducers";
import {
  productListReducer,
  toggelButtonReducer,
} from "./store/Reducers/homeReducers";

import logInSlice from "./store/Reducers/logInSlice";

const initialState = {};
const reducer = combineReducers({
  logInSlice: logInSlice,
  productList: productListReducer,
  toggleButton: toggelButtonReducer,
  filterDropDownList: filterListReducer,
  clickFilter: filterClickReducer,
  keyWord: filterKeyWordReducer,
  exportExcelsheet:exportExcelReducer,
});

const composerEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  initialState,
  composerEnhancer(applyMiddleware(thunk))
);

export default store;
