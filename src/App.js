import React, { useState } from "react";
import LogIn from "./components/LogIn";
import Home from "./components/Home";

import { useDispatch, useSelector } from "react-redux";
import { signIn } from "./store/Reducers/logInSlice";

function App() {
  const logInSliceData = useSelector((state) => state.logInSlice);
  const [passwordValue, setPasswordValue] = useState("");
  const [userNameValue, setuserNameValue] = useState("");

  // const { isLoggedIn } = logInSliceData;
  const isLoggedIn = 1;
  const dispatch = useDispatch();

  const logInSubmit = () => {
    dispatch(signIn(userNameValue, passwordValue));
  };

  return (
    <div>
      {isLoggedIn ? (
        <Home />
      ) : (
        <LogIn
          logInSubmit={logInSubmit}
          setPasswordValue={setPasswordValue}
          setuserNameValue={setuserNameValue}
        />
      )}
    </div>
  );
}

export default App;
