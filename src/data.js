const data = {
    products:[
        {
            _id:'1',
            name: '1Kompönent Fibre Reinforced Waterproof Membrane',
            image: 'images/1.png',
            itemCode: '1222490',
            itemType: 'Waterproofing',
            itemSubDivision: '4631839',
            itemMajorGroup: '463139',
            effectiveDate: '12 Apr 2021',
            itemMinorGroup: '4069123',
            salesperson: 'James Carson'
        },
        {
            _id:'2',
            name: '2 Part Cementitious Waterproofing Membrane',
            image: 'images/2.png',
            itemCode: '12224904',
            itemType: 'Waterproofing',
            itemSubDivision: '4631839',
            itemMajorGroup: '463139',
            effectiveDate: '12 Apr 2021',
            itemMinorGroup: '4069123',
            salesperson: 'James Carson'
        },
        {
            _id:'3',
            name: 'Addflextra Tile Adhesive',
            image: 'images/3.png',
            itemCode: '12224904',
            itemType: 'Waterproofing',
            itemSubDivision: '4631839',
            itemMajorGroup: '463139',
            effectiveDate: '12 Apr 2021',
            itemMinorGroup: '4069123',
            salesperson: 'James Carson'
        },
        {
            _id:'4',
            name: 'Addflextra Tile Adhesive',
            image: 'images/4.png',
            itemCode: '12224904',
            itemType: 'Waterproofing',
            itemSubDivision: '4631839',
            itemMajorGroup: '463139',
            effectiveDate: '13 Apr 2021',
            itemMinorGroup: '4069123',
            salesperson: 'James Carson'
        },
    ]
}

export default data;